import { IPlayerEntity } from "../../store/reducers/players"
import CardFactory from '../Cards'
import './styles.scss'

interface IPlayersCardsProps {
    player: IPlayerEntity
}

const PlayersCards = (props: IPlayersCardsProps) => {
    const { player } = props
    const { business } = player

    const zoom =  (window.innerHeight * 0.20) / 470
    return <div className="playersCardsContainer">
        {business.map((item) => <div className="playersCardsContainer__card" style={{zoom}}><CardFactory name={item.name} /></div> )}
    </div>
} 

export default PlayersCards