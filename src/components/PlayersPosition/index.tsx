import { IPlayerEntity } from "../../store/reducers/players"
import PlayersCards from "../PlayersCards"
import './styles.scss'

interface IPlayersPositionProps {
    players: IPlayerEntity[]
}

const PlayersPosition = (props: IPlayersPositionProps) => {

    const { players } = props

    const playersAmount = players.length

    switch (playersAmount) {
        case 3: return <div className="playersContainer playersContainer-3">
            <div className="player1">
                <PlayersCards player={players[0]}/>
            </div>
            <div className="player2">
        Your friend
            </div>
            <div className="player3">
        My friend
            </div>
        </div>
        case 4: return <div className="playersContainer playersContainer-4">
            <div className="player1">

            </div>
            <div className="player2">

            </div>
            <div className="player3">

            </div>
            <div className="player4">

            </div>
        </div>
        case 5: return <div className="playersContainer playersContainer-5">
            <div className="player1">

            </div>
            <div className="player2">

            </div>
            <div className="player3">

            </div>
            <div className="player4">

            </div>
            <div className="player5">

            </div>
        </div>
        default: return <div className="playersContainer playersContainer-2">
            <div className="player1">

            </div>
            <div className="player2">

            </div>
        </div>

    }
}

export default PlayersPosition