interface IDiceProps {
    value: number
}

const Dice = (props: IDiceProps) => {

    const { value } = props

    switch (value) {
        case 1: return <div className="dice">
            <div className="row"><div className="dot"></div></div>
        </div>
        case 2: return <div className="dice">
            <div className="row"><div className="dot" /><div className="dot--fake" /><div className="dot--fake" /></div>
            <div className="row"><div className="dot--fake" /></div>
            <div className="row"><div className="dot--fake" /><div className="dot--fake" /><div className="dot" /></div>
        </div>
        case 3: return <div className="dice">
            <div className="row"><div className="dot" /><div className="dot--fake" /><div className="dot--fake" /></div>
            <div className="row"><div className="dot--fake" /><div className="dot" /><div className="dot--fake" /></div>
            <div className="row"><div className="dot--fake" /><div className="dot--fake" /><div className="dot" /></div>
        </div>
        case 4: return <div className="dice">
            <div className="row"><div className="dot--biggerMargin" /><div className="dot--biggerMargin" /></div>
            <div className="row"><div className="dot--fake"></div></div>
            <div className="row"><div className="dot--biggerMargin" /><div className="dot--biggerMargin" /></div>
        </div>
        case 5: return <div className="dice">
            <div className="row"><div className="dot--biggerMargin" /><div className="dot--biggerMargin" /></div>
            <div className="row"><div className="dot--biggerMargin" /></div>
            <div className="row"><div className="dot--biggerMargin" /><div className="dot--biggerMargin" /></div>
        </div>
        case 6: return <div className="dice">
            <div className="row"><div className="dot--biggerMargin" /><div className="dot--biggerMargin" /></div>
            <div className="row"><div className="dot--biggerMargin" /><div className="dot--biggerMargin" /></div>
            <div className="row"><div className="dot--biggerMargin" /><div className="dot--biggerMargin" /></div>
        </div>
        default: return <div className="dice"></div>
    }
}

export default Dice
