import { useState } from "react"
import Dice from "./Dice"
import './styles.scss'

enum EDicesAmount {
    ONE = '1',
    TWO = '2',
}

const Dices = () => {

    const [dicesAmount, setDicesAmount] = useState<EDicesAmount>(EDicesAmount.ONE)
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [canRoll, setCanRoll] = useState(true)                  // TODO: disable after you rolled once or it's not your turn
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [canRollTwoDices, setCanRollTwoDice] = useState(true); // TODO: take this from props later

    const [val1, setVal1] = useState(0);
    const [val2, setVal2] = useState(0)

    const onGenerateValues = () => {
        setVal1(Math.ceil(Math.random() * 6))
        setVal2(Math.ceil(Math.random() * 6))
    }

    return <div className="dicesContainer">
        <div className="chooseAmount">
            <div
                className={
                    `chooseAmount__block 
                            ${dicesAmount === EDicesAmount.ONE ? 'chooseAmount__block--active' : ''}`
                }
                onClick={() => setDicesAmount(EDicesAmount.ONE)}
            >
                {EDicesAmount.ONE}
            </div>
            <div
                className={
                    `chooseAmount__block 
                    ${canRollTwoDices ? '' : 'chooseAmount__block--disabled'}
                    ${dicesAmount === EDicesAmount.TWO ? 'chooseAmount__block--active' : ''}`
                }
                onClick={() => canRollTwoDices && setDicesAmount(EDicesAmount.TWO)}
            >
                {EDicesAmount.TWO}
            </div>
        </div>

        <div className="dicesBoard">
            {dicesAmount === EDicesAmount.ONE ? <Dice value={val1} /> : <><Dice value={val1} /> <Dice value={val2} /></>}
        </div>

        <div className='rollButton' onClick={canRoll ? onGenerateValues : undefined}>Roll</div>

    </div>
}

export default Dices