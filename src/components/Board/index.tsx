import { connect } from 'react-redux';
import { IStore } from '../../store/reducers';
import { IPlayerEntity, IPlayersState } from '../../store/reducers/players';
import PlayersPosition from '../PlayersPosition';
import './styles.scss'

interface IBoardProps {
    players: IPlayerEntity[]
}

const Board = (props: IBoardProps) => {
    const { players } = props;
    return <div className="board">
        <PlayersPosition players={players} />
    </div>
}

const getActivePlayers = (players: IPlayersState) => {
    return Object.keys(players)
        .map((item) => (players as any)[item] as IPlayerEntity)
        .filter((player) => player.isPlaying)
}

const mapStateToProps = (state: IStore) => ({
    players: getActivePlayers(state.playersReducer)
})

export default connect(mapStateToProps)(Board);