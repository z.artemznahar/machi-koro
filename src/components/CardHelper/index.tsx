import { connect } from "react-redux"
import CardFactory from '../Cards'
import { IStore } from "../../store/reducers"
import { ECardName } from "../../helpers/cards"
import './styles.scss'
import { actionRemoveCard } from "../../store/actions/cardHelper"

interface ICardHelperProps {
    cardName: ECardName | undefined
    actionRemoveCard: () => void
}

const CardHelper = (props: ICardHelperProps) => {
    const { cardName, actionRemoveCard } = props
    if (cardName)
        return <>
            <div className="cardHelper__backdrop" onClick={() => actionRemoveCard()}/>
            <div className="cardHelper">
                <CardFactory name={cardName} />
            </div>
        </>
    return null
}

const mapStateToProps = (state: IStore) => ({
    cardName: state.cardHelperReducer.currentCard
})

export default connect(mapStateToProps, { actionRemoveCard })(CardHelper)