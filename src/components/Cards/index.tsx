import { connect } from "react-redux"
import { cardsFullInfo, ECardName } from "../../helpers/cards";
import { actionAddCard } from "../../store/actions/cardHelper";
import { IStore } from "../../store/reducers"
import { ELocale } from "../../store/reducers/preferences";

import './styles.scss'

interface ICardProps {
    name: ECardName,
    locale: ELocale
    actionAddCard: (cardName: ECardName) => void
}


const CardFactory = (props: ICardProps) => {
    const { name, locale } = props;
    const { actionAddCard } = props

    // const kind = checkKind(name) // green, blue, red etc
    const card = cardsFullInfo[locale][name]
    const win = card.winCondition.join('-')

    const [desc1, desc2] = card.description.split('.')

    return <div 
        className="cardContainer"
        onDoubleClick={() => {
            actionAddCard(name)
        }}
    >
        <div className={`card card--${card.kind}`}>
            <div className="winCondition">
                <strong>{win}</strong>
            </div>

            <div className="cardBackgroundImage">
                <img src={card.image} alt="" className="cardBackgroundImage__image"></img>
                <strong className={`cardType  cardType--${card.kind}`}>
                    <div className={`cardType__icon cardType--${card.type} cardType__icon--${card.kind}`} />
                    <div className={`cardType__text text--${card.kind}`}>
                        {card.title}
                    </div>
                </strong>
            </div>

            <div className="cardDescription">
                <div className="cardDescription__line1">{desc1}.</div>
                <div className="cardDescription__line2">{desc2}.</div>
            </div>

            <div className="cardPrice">
                <div className="inside">
                    <div className="superInside">
                        <strong>{card.price}</strong>
                    </div>
                </div>
            </div>

        </div>
    </div>
}

const mapStateToProps = (state: IStore) => ({
    locale: state.preferencesReducer.language
})

export default connect(mapStateToProps, { actionAddCard })(CardFactory)