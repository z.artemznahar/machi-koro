import { useState } from "react"
import { connect } from "react-redux"
import { actionChangeLanguage } from "../../store/actions/preferences"
import { IStore } from "../../store/reducers"
import { ELocale } from "../../store/reducers/preferences"

import './styles.scss'

interface ILanguageProps {
    language: ELocale
    actionChangeLanguage: (value: ELocale) => void
}

const Language = (props: ILanguageProps) => {
    const { language, actionChangeLanguage } = props
    const [isActiveSelect, setActiveSelect] = useState(false)
    const keys = Object.keys(ELocale)

    const onChangeLanguage = (lang: ELocale) => {
        setActiveSelect(false)
        actionChangeLanguage(lang);
    }

    return <div className="language" onMouseDown={(e) => e.preventDefault()}>
        <div 
            className="language__item" 
            onClick={() => setActiveSelect(!isActiveSelect)}
        >
            {language}
        </div>
        {isActiveSelect && keys
            .filter((lang) => (ELocale as any)[lang] !== language)
            .map((lang) => 
            <div 
                key={lang} 
                className="language__item"
                onClick={() => onChangeLanguage((ELocale as any)[lang])}
            >
                {lang}
            </div>)
        }
    </div>
}

const mapStateToProps = (state: IStore) => ({
    language: state.preferencesReducer.language
})
export default connect(mapStateToProps, { actionChangeLanguage })(Language)