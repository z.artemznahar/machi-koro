import { createStore, compose, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import reducers from './reducers';

const sagaMiddleware = createSagaMiddleware();

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  reducers,
  composeEnhancers(
    applyMiddleware(sagaMiddleware)
  )
);

// sagaMiddleware.run(accountsSaga);


// store.dispatch(actionGetRates());

/**
 * Subscribe to web3 events
 * React to each new ETH block mined
 */
// const subscription = new BlocksSubscription({
//   eth: (eth: number) => store.dispatch(actionEthBlockMined(eth)),
//   bsc: (bsc: number) => store.dispatch(actionBscBlockMined(bsc)),
//   polygon: (polygon: number) => store.dispatch(actionPolygonBlockMined(polygon)),
//   gas: (gas: IGasState) => store.dispatch(actionGasPrice(gas)),
//   newpairs: (message: string) => store.dispatch(actionCreateNotification({ message })),
//   trade: (trade: IDexTrade) => store.dispatch(actionBeforeAppendTrade(trade))
// });

export {
  store,
//   subscription
};