import { ECardName } from "../../helpers/cards";
import { ADD_CARD, REMOVE_CARD } from "../actions/cardHelper";

export interface ICardHelperState {
  currentCard: ECardName | undefined;
}

const initialState = {
  currentCard: undefined,
};

const cardHelperReducer = (state: ICardHelperState = initialState, action: any) => {
  switch (action.type) {
    case ADD_CARD:
      return {
        currentCard: action.payload,
      };
    case REMOVE_CARD:
      return {
        currentCard: undefined,
      };
    default:
      return state;
  }
};

export default cardHelperReducer;
