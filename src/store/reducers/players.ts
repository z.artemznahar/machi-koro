import { ECardName } from "../../helpers/cards";


export interface IPlayerEntity {
    isPlaying: boolean,
    isTurn: boolean,
    money: number
    business: IBusiness[]
    name: string
}

interface IBusiness {
    name: ECardName
    amount: number
}

export interface IPlayersState {
    player1: IPlayerEntity
    player2: IPlayerEntity
    player3: IPlayerEntity
    player4: IPlayerEntity
    player5: IPlayerEntity
}

const initialState = {
    player1: {
        isPlaying: true,
        isTurn: false,
        money: 3,
        business: [
            { name: ECardName.RESTAURANT, amount: 1 },
            { name: ECardName.BAKERY, amount: 1 },
            { name: ECardName.WHEAT_FIELD, amount: 1 },
            { name: ECardName.APPLE_ORCHARD, amount: 1 },
            { name: ECardName.CHEESE_DIARY, amount: 2},
            { name: ECardName.TRAWLER, amount: 2},
            { name: ECardName.RESTAURANT, amount: 1 },
            { name: ECardName.BAKERY, amount: 1 },
            { name: ECardName.WHEAT_FIELD, amount: 1 },
            { name: ECardName.APPLE_ORCHARD, amount: 1 },
            { name: ECardName.CHEESE_DIARY, amount: 2},
            { name: ECardName.TRAWLER, amount: 2},
        ],
        name: 'Яна цист',
    } ,
    player2: {
        isPlaying: true,
        isTurn: false,
        money: 3,
        business: [],
        name: 'Яги Тлер',
    } ,
    player3: {
        isPlaying: true,
        isTurn: false,
        money: 3,
        business: [],
        name: 'Даюва Налл'
    } ,
    player4: {
        isPlaying: false,
        isTurn: false,
        money: 3,
        business: [],
        name: 'Матье Балл',
    } ,
    player5: {
        isPlaying: false,
        isTurn: false,
        money: 3,
        business: [],
        name: 'Яспер Маглот',
    },
}

const playersReducer = (state: IPlayersState = initialState, action: any) => {
    switch (action.type) {
    // case CHANGE_LOCALE:
    //   return {
    //     ...state,
    //     language: action.payload
    //   };
    default:
      return state;
    }
  };
  
  export default playersReducer;
  