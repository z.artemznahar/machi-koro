import { CHANGE_LOCALE } from "../actions/preferences";

export enum ELocale {
    RU = 'ru',
    EN = 'en',
    UA = 'ua'
}
export interface IPreferencesState {
    language: ELocale
}

const initialState = {
    language: localStorage.getItem('lang') as ELocale || (ELocale.EN)
}

const preferencesReducer = (state: IPreferencesState = initialState, action: any) => {
    switch (action.type) {
    case CHANGE_LOCALE:
      localStorage.setItem('lang', action.payload)
      return {
        ...state,
        language: action.payload
      };
    default:
      return state;
    }
  };
  
  export default preferencesReducer;
  