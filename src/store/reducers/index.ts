import { combineReducers } from 'redux';
import cardsReducer, { ICardsState } from './cards';
import preferencesReducer, { IPreferencesState } from './preferences';
import playersReducer, { IPlayersState } from './players';
import cardHelperReducer, { ICardHelperState } from './cardHelper';


export interface IStore {
    preferencesReducer: IPreferencesState
    cardsReducer: ICardsState
    playersReducer: IPlayersState
    cardHelperReducer: ICardHelperState
}

export default combineReducers({
    preferencesReducer,
    cardsReducer,
    playersReducer,
    cardHelperReducer
});