import { action } from "typesafe-actions";

const entity = "players";
const RESET_PLAYERS = `${entity}reset`;

const actionResetPlayers = () => action(RESET_PLAYERS);

export { RESET_PLAYERS, actionResetPlayers };
