import { action } from 'typesafe-actions'
import { ELocale } from '../reducers/preferences'

const entity = 'preferences'
const CHANGE_LOCALE = `${entity}change_language`

const actionChangeLanguage = (payload: ELocale) => 
    action(CHANGE_LOCALE, payload)

export { CHANGE_LOCALE, actionChangeLanguage }