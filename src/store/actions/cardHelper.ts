import { action } from "typesafe-actions";
import { ECardName } from "../../helpers/cards";

const entity = "cardHelper";
const ADD_CARD = `${entity}addCard`;
const REMOVE_CARD = `${entity}removeCard`;

const actionAddCard = (cardName: ECardName) => action(ADD_CARD, cardName);
const actionRemoveCard = () => action(REMOVE_CARD);

export { ADD_CARD, REMOVE_CARD, actionAddCard, actionRemoveCard };
