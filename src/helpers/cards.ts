import { ELocale } from "../store/reducers/preferences";
import placeholder  from '../images/placeholder.jpeg'

// enums
enum ECardKind {
  GREEN = "green",
  BLUE = "blue",
  RED = "red",
  VIOLET = "violet",
  YELLOW = "yellow",
}

enum ECardName {
  // Green
  BAKERY = "Bakery",
  STORE = "Store",
  FLOWER_SHOP = 'Flower shop',
  CHEESE_DIARY = 'Cheese diary',
  FURNITURE_FACTORY = 'Furniture factory',
  FRUIT_MARKET = 'Fruit market',
  // Blue
  WHEAT_FIELD = 'Wheat field',
  FARM = 'Farm',
  FLOWER_GARDEN = 'Flower garden',
  FOREST = 'Forest',
  FISHING_BOAT = 'Fishing boat',
  MINE = 'Mine',
  APPLE_ORCHARD = 'Apple orchard',
  TRAWLER = 'Trawler',
  // Red
  SUSHIBAR = 'Sushi bar',
  CAFE = 'Cafe',
  PIZZERIA = 'Pizzeria',
  DINERS = 'Diners',
  RESTAURANT = 'Restaurant',
}

enum ECardType {
  SHOP = "shop",  // done
  FACTORY = 'factory', // done
  FRUIT = 'fruit',  // done
  WHEAT = 'wheat', // done
  FARM = 'farm',  // done
  GEAR = 'gear',    // done
  FISHING = 'fishing', // done
  CAFE = 'cafe' // done
}

// constants
const cardsCommonInfo = {
  [ECardName.BAKERY]: {
    id: 1,
    type: ECardType.SHOP,
    price: 1,
    winCondition: [2, 3],
    image: placeholder,
    kind: ECardKind.GREEN,
  },
  [ECardName.STORE]: {
    id: 2,
    type: ECardType.SHOP,
    price: 2,
    winCondition: [4],
    image: placeholder,
    kind: ECardKind.GREEN,
  },
  [ECardName.FLOWER_SHOP]: {
    id: 3,
    type: ECardType.SHOP,
    price: 1,
    winCondition: [6],
    image: placeholder,
    kind: ECardKind.GREEN,
  },
  [ECardName.CHEESE_DIARY]: {
    id: 4,
    type: ECardType.FACTORY,
    price: 5,
    winCondition: [7],
    image: placeholder,
    kind: ECardKind.GREEN,
  },
  [ECardName.FURNITURE_FACTORY]: {
    id: 5,
    type: ECardType.FACTORY,
    price: 3,
    winCondition: [8],
    image: placeholder,
    kind: ECardKind.GREEN,
  },
  [ECardName.FRUIT_MARKET]: {
    id: 6,
    type: ECardType.FRUIT,
    price: 2,
    winCondition: [11, 12],
    image: placeholder,
    kind: ECardKind.GREEN,
  },
  [ECardName.WHEAT_FIELD]: {
    id: 7,
    type: ECardType.WHEAT,
    price: 1,
    winCondition: [1],
    image: placeholder,
    kind: ECardKind.BLUE,
  },
  [ECardName.FARM]: {
    id: 8,
    type: ECardType.FARM,
    price: 1,
    winCondition: [2],
    image: placeholder,
    kind: ECardKind.BLUE
  },
  [ECardName.FLOWER_GARDEN]: {
    id: 9,
    type: ECardType.WHEAT,
    price: 2,
    winCondition: [4],
    image: placeholder,
    kind: ECardKind.BLUE
  },
  [ECardName.FOREST]: {
    id: 10,
    type: ECardType.GEAR,
    price: 3,
    winCondition: [5],
    image: placeholder,
    kind: ECardKind.BLUE
  },
  [ECardName.FISHING_BOAT]: {
    id: 11,
    type: ECardType.FISHING,
    price: 2,
    winCondition: [8],
    image: placeholder,
    kind: ECardKind.BLUE
  },
  [ECardName.MINE]: {
    id: 12,
    type: ECardType.GEAR,
    price: 6,
    winCondition: [9],
    image: placeholder,
    kind: ECardKind.BLUE
  },
  [ECardName.APPLE_ORCHARD]: {
    id: 13,
    type: ECardType.WHEAT,
    price: 3,
    winCondition: [10],
    image: placeholder,
    kind: ECardKind.BLUE
  },
  [ECardName.TRAWLER]: {
    id: 14,
    type: ECardType.GEAR,
    price: 5,
    winCondition: [12,14],
    image: placeholder,
    kind: ECardKind.BLUE
  },
  [ECardName.TRAWLER]: {
    id: 14,
    type: ECardType.GEAR,
    price: 5,
    winCondition: [12,14],
    image: placeholder,
    kind: ECardKind.BLUE
  },
  [ECardName.SUSHIBAR]: {
    id: 15,
    type: ECardType.CAFE,
    price: 2,
    winCondition: [1],
    image: placeholder,
    kind: ECardKind.RED
  },
  [ECardName.CAFE]: {
    id: 16,
    type: ECardType.CAFE,
    price: 2,
    winCondition: [3],
    image: placeholder,
    kind: ECardKind.RED
  },
  [ECardName.PIZZERIA]: {
    id: 17,
    type: ECardType.CAFE,
    price: 1,
    winCondition: [7],
    image: placeholder,
    kind: ECardKind.RED
  },
  [ECardName.DINERS]: {
    id: 18,
    type: ECardType.CAFE,
    price: 2,
    winCondition: [8],
    image: placeholder,
    kind: ECardKind.RED
  },
  [ECardName.RESTAURANT]: {
    id: 19,
    type: ECardType.CAFE,
    price: 3,
    winCondition: [9, 10],
    image: placeholder,
    kind: ECardKind.RED
  },
};


const cardsFullInfo = {
  [ELocale.RU]: {
    [ECardName.BAKERY]: {
      ...cardsCommonInfo[ECardName.BAKERY],
      title: "Пекарня",
      description: "Возьмите 1 монету из банка. В свой ход",
    },
    [ECardName.STORE]: {
      ...cardsCommonInfo[ECardName.STORE],
      title: "Магазин",
      description: "Возьмите 3 монеты из банка. В свой ход",
    },
    [ECardName.FLOWER_SHOP]: {
      ...cardsCommonInfo[ECardName.FLOWER_SHOP],
      title: "Цветочный магазин",
      description: "Возьмите 1 монету из банка за каждый ваш \"Цветник\". В свой ход",
    },
    [ECardName.CHEESE_DIARY]: {
      ...cardsCommonInfo[ECardName.CHEESE_DIARY],
      title: "Сыроварня",
      description: "Возьмите 3 монеты из банка за каждое ваше предприятние с символом *бык*. В свой ход",
    },
    [ECardName.FURNITURE_FACTORY]: {
      ...cardsCommonInfo[ECardName.FURNITURE_FACTORY],
      title: "Мебельная фабрика",
      description: "Возьмите 3 монеты из банка за каждое ваше предприятние с символом *шестерня*. В свой ход",
    },
    [ECardName.FRUIT_MARKET]: {
      ...cardsCommonInfo[ECardName.FRUIT_MARKET],
      title: "Фруктовый рынок",
      description: "Возьмите 3 монеты из банка за каждое ваше предприятние с символом *пшеница*. В свой ход",
    },
    [ECardName.WHEAT_FIELD]: {
      ...cardsCommonInfo[ECardName.WHEAT_FIELD],
      title: "Пшеничное поле",
      description: "Возьмите 1 монету из банка. В ход любого игрока",
    },
    [ECardName.FARM]: {
      ...cardsCommonInfo[ECardName.FARM],
      title: "Ферма",
      description: "Возьмите 1 монету из банка. В ход любого игрока",
    },
    [ECardName.FLOWER_GARDEN]: {
      ...cardsCommonInfo[ECardName.FLOWER_GARDEN],
      title: "Цветник",
      description: "Возьмите 1 монету из банка. В ход любого игрока",
    },
    [ECardName.FOREST]: {
      ...cardsCommonInfo[ECardName.FOREST],
      title: "Лес",
      description: "Возьмите 1 монету из банка. В ход любого игрока",
    },
    [ECardName.FISHING_BOAT]: {
      ...cardsCommonInfo[ECardName.FISHING_BOAT],
      title: "Рыбацкий баркас",
      description: "Если у вас есть порт, возьмите 3 монеты из банка. В ход любого игрока",
    },
    [ECardName.MINE]: {
      ...cardsCommonInfo[ECardName.MINE],
      title: "Шахта",
      description: "Возьмите 5 монет из банка. В ход любого игрока",
    },
    [ECardName.APPLE_ORCHARD]: {
      ...cardsCommonInfo[ECardName.APPLE_ORCHARD],
      title: "Яблоневый сад",
      description: "Возьмите 3 монеты из банка. В ход любого игрока",
    },
    [ECardName.TRAWLER]: {
      ...cardsCommonInfo[ECardName.TRAWLER],
      title: "Траулер",
      description: "Активный игрое бросает 2 кубика, если у вас есть \"Порт\" возьмите из банка столько монет, сколько выпало на кубиках. В ход любого игрока",
    },
    [ECardName.SUSHIBAR]: {
      ...cardsCommonInfo[ECardName.SUSHIBAR],
      title: "Суши-бар",
      description: "Если у вас есть \"Порт\" возьмите 3 монеты у игрока, бросившего кубики. В ход другого игрока",
    },
    [ECardName.CAFE]: {
      ...cardsCommonInfo[ECardName.CAFE],
      title: "Кафе",
      description: "Возьмите 1 монету у игрока, бросившего кубики. В ход другого игрока",
    },
    [ECardName.PIZZERIA]: {
      ...cardsCommonInfo[ECardName.PIZZERIA],
      title: "Пиццерия",
      description: " Возьмите 1 монету у игрока, бросившего кубики. В ход другого игрока",
    },
    [ECardName.DINERS]: {
      ...cardsCommonInfo[ECardName.DINERS],
      title: "Закусочная",
      description: " Возьмите 1 монету у игрока, бросившего кубики. В ход другого игрока",
    },
    [ECardName.RESTAURANT]: {
      ...cardsCommonInfo[ECardName.RESTAURANT],
      title: "Ресторан",
      description: "Возьмите 2 монеты у игрока монеты у игрока, бросившего кубики. В ход другого игрока",
    },
  },
  [ELocale.EN]: {
    [ECardName.BAKERY]: {
      ...cardsCommonInfo[ECardName.BAKERY],
      title: "Bakery",
      description: "Take 1 coin from the bank. On your turn",
    },
    [ECardName.STORE]: {
      ...cardsCommonInfo[ECardName.STORE],
      title: "Store",
      description: "Take 3 coins from the bank. On your turn",
    },
    [ECardName.FLOWER_SHOP]: {
      ...cardsCommonInfo[ECardName.FLOWER_SHOP],
      title: "Fruit market",
      description: "Take 1 coin from the bank for each of your \"Flower garden\". On your turn",
    },
    [ECardName.CHEESE_DIARY]: {
      ...cardsCommonInfo[ECardName.CHEESE_DIARY],
      title: "Cheese diary",
      description: "Take 3 coins from the bank for each of your manudacture with *bull* sign. On your turn",
    },
    [ECardName.FURNITURE_FACTORY]: {
      ...cardsCommonInfo[ECardName.FURNITURE_FACTORY],
      title: "Furniture factory",
      description: "Take 3 coins from the bank for each of your manudacture with *gear* sign. On your turn",
    },
    [ECardName.FRUIT_MARKET]: {
      ...cardsCommonInfo[ECardName.FRUIT_MARKET],
      title: "Fruit market",
      description: "Take 2 coins from the bank for each of your manudacture with *wheat* sign. On your turn",
    },
    [ECardName.WHEAT_FIELD]: {
      ...cardsCommonInfo[ECardName.WHEAT_FIELD],
      title: "Wheat field",
      description: "Take 1 coin from the bank. On anyone's turn",
    },
    [ECardName.FARM]: {
      ...cardsCommonInfo[ECardName.FARM],
      title: "Farm",
      description: "Take 1 coin from the bank. On anyone's turn",
    },
    [ECardName.FLOWER_GARDEN]: {
      ...cardsCommonInfo[ECardName.FLOWER_GARDEN],
      title: "Farm",
      description: "Take 1 coin from the bank. On anyone's turn",
    },
    [ECardName.FOREST]: {
      ...cardsCommonInfo[ECardName.FOREST],
      title: "Forest",
      description: "Take 1 coin from the bank. On anyone's turn",
    },
    [ECardName.FISHING_BOAT]: {
      ...cardsCommonInfo[ECardName.FISHING_BOAT],
      title: "Fishing boat",
      description: "Take 3 coins from the bank if you own \"Port\". On anyone's turn",
    },
    [ECardName.MINE]: {
      ...cardsCommonInfo[ECardName.MINE],
      title: "Mine",
      description: "Take 5 coins from the bank. On anyone's turn",
    },
    [ECardName.APPLE_ORCHARD]: {
      ...cardsCommonInfo[ECardName.APPLE_ORCHARD],
      title: "Apple orchard",
      description: "Take 3 coins from the bank. On anyone's turn",
    },
    [ECardName.TRAWLER]: {
      ...cardsCommonInfo[ECardName.TRAWLER],
      title: "Trawler",
      description: "The active player rolls 2 dices, if you own \"Port\" take the resulting amount of coins from the bank. On anyone's turn",
    },
    [ECardName.SUSHIBAR]: {
      ...cardsCommonInfo[ECardName.SUSHIBAR],
      title: "Sushi bar",
      description: "If you have \"Port\" take 3 coins from the active player. On other players' turn",
    },
    [ECardName.CAFE]: {
      ...cardsCommonInfo[ECardName.CAFE],
      title: "Sushi bar",
      description: "Take 1 coin from the active player. On other players' turn",
    },
    [ECardName.PIZZERIA]: {
      ...cardsCommonInfo[ECardName.CAFE],
      title: "Pizzeria",
      description: "Take 1 coin from the active player. On other players' turn",
    },
    [ECardName.DINERS]: {
      ...cardsCommonInfo[ECardName.CAFE],
      title: "Diners",
      description: "Take 1 coin from the active player. On other players' turn",
    },
    [ECardName.RESTAURANT]: {
      ...cardsCommonInfo[ECardName.RESTAURANT],
      title: "RESTAURANT",
      description: "Take 2 coins from the active player. On other players' turn",
    },
  },
  [ELocale.UA]: {
    [ECardName.BAKERY]: {
      ...cardsCommonInfo[ECardName.BAKERY],
      title: "Пекарня",
      description: "Візьміть 1 монету з банку. У свій хід",
    },
    [ECardName.STORE]: {
      ...cardsCommonInfo[ECardName.STORE],
      title: "Магазин",
      description: "Візьміть 3 монети з банку. У свій хід",
    },
    [ECardName.FLOWER_SHOP]: {
      ...cardsCommonInfo[ECardName.FLOWER_SHOP],
      title: "Квітковий магазин",
      description: "Візьміть 1 монету з банку за кожен ваш \"Квітник\". У свій хід",
    },
    [ECardName.CHEESE_DIARY]: {
      ...cardsCommonInfo[ECardName.CHEESE_DIARY],
      title: "Сироварня",
      description: "Візьміть 3 монети з банку за кожне ваше підприємство із символом *бик*. У свій хід",
    },
    [ECardName.FURNITURE_FACTORY]: {
      ...cardsCommonInfo[ECardName.FURNITURE_FACTORY],
      title: "Мебельна фабрика",
      description: "Візьміть 3 монети з банку за кожне ваше підприємство із символом *Шестерня*. У свій хід",
    },
    [ECardName.FRUIT_MARKET]: {
      ...cardsCommonInfo[ECardName.FRUIT_MARKET],
      title: "Фруктовий ринок",
      description: "Візьміть 3 монети з банку за кожне ваше підприємство із символом *Пшениця*. У свій хід",
    },
    [ECardName.WHEAT_FIELD]: {
      ...cardsCommonInfo[ECardName.WHEAT_FIELD],
      title: "Пшеничне поле",
      description: "Візьміть 1 монету з банку. У хід будь-якого гравця",
    },
    [ECardName.FARM]: {
      ...cardsCommonInfo[ECardName.FARM],
      title: "Ферма",
      description: "Візьміть 1 монету з банку. У хід будь-якого гравця",
    },
    [ECardName.FLOWER_GARDEN]: {
      ...cardsCommonInfo[ECardName.FLOWER_GARDEN],
      title: "Ферма",
      description: "Візьміть 1 монету з банку. У хід будь-якого гравця",
    },[ECardName.FOREST]: {
      ...cardsCommonInfo[ECardName.FOREST],
      title: "Ліс",
      description: "Візьміть 1 монету з банку. У хід будь-якого гравця",
    },
    [ECardName.FISHING_BOAT]: {
      ...cardsCommonInfo[ECardName.FISHING_BOAT],
      title: "Рибальский човен",
      description: "Якщо у вас є \"Порт\" візьміть 3 монети з банку. У хід будь-якого гравця",
    },
    [ECardName.MINE]: {
      ...cardsCommonInfo[ECardName.MINE],
      title: "Шахта",
      description: "Візьміть 5 монет з банку. У хід будь-якого гравця",
    },
    [ECardName.APPLE_ORCHARD]: {
      ...cardsCommonInfo[ECardName.APPLE_ORCHARD],
      title: "Яблуневий сад",
      description: "Візьміть 3 монети з банку. У хід будь-якого гравця",
    },
    [ECardName.TRAWLER]: {
      ...cardsCommonInfo[ECardName.TRAWLER],
      title: "Траулер",
      description: "Активний гравець кидає кубики, якщо у вас є \"Порт\", візьміть стільки монет з банку, скільки випало на кубиках. У хід будь-якого гравця",
    },
    [ECardName.SUSHIBAR]: {
      ...cardsCommonInfo[ECardName.SUSHIBAR],
      title: "Суші-бар",
      description: "Якщо у вас є \"Порт\", візьміть 3 монети у гравця, що кинув кубики. У хід іншого гравця",
    },
    [ECardName.CAFE]: {
      ...cardsCommonInfo[ECardName.CAFE],
      title: "Суші-бар",
      description: "Візьміть 1 монету у гравця, що кинув кубики. У хід іншого гравця",
    },
    [ECardName.PIZZERIA]: {
      ...cardsCommonInfo[ECardName.PIZZERIA],
      title: "Піцерія",
      description: "Візьміть 1 монету у гравця, що кинув кубики. У хід іншого гравця",
    },
    [ECardName.DINERS]: {
      ...cardsCommonInfo[ECardName.DINERS],
      title: "Закусочна",
      description: "Візьміть 1 монету у гравця, що кинув кубики. У хід іншого гравця",
    },
    [ECardName.RESTAURANT]: {
      ...cardsCommonInfo[ECardName.RESTAURANT],
      title: "Ресторан",
      description: "Візьміть 2 монети у гравця, що кинув кубики. У хід іншого гравця",
    },
  },
};

// functions

export { cardsFullInfo, ECardName };
