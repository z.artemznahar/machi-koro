import Board from "./components/Board";
import CardHelper from "./components/CardHelper";
import Dices from "./components/Dices";
import Language from "./components/Language";
// import { ECardName } from "./helpers/cards";
// import CardFactory from './components/Cards'

function App() {
  return (
    <div className="App">
      {/* <CardFactory name={ECardName.BAKERY}/> */}
      <Board />
      <Dices />
      <Language />
      <CardHelper />
    </div>
  );
}

export default App;
